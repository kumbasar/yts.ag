# YTS.ag 

yts.py is a simple tool which fetches torrent movies using https://yts.am/api 

### Installation

Only Python3 is required.

### Execution

```sh
$ python3 yts.py
```

### Output
```sh
ghost:yts.ag kumbasar$ python3 yts.py
Title    : Jago: A Life Underwater
Year     : 2015
Rating   : 7.5
Summary  : Documentary about Rohani, an 80-year-old hunter who dives like a fish on
           a single breath, descending to great depths for several minutes. Set
           against the spectacular backdrop of the Togian Islands in Indonesia
           where he grew up, this award-winning film recreates events that capture
           the extraordinary turning points in his life, as a hunter and as a man.
URL      : https://yts.am/movie/jago-a-life-underwater-2015
Torrent  : https://yts.am/torrent/download/E265EE51967592D9632B37042AEE801CB52A4D4E
Quality  : 1080p
###################################################################################
Title    : Patton Oswalt: Annihilation
Year     : 2017
Rating   : 7.3
Summary  : Patton Oswald, despite a personal tragedy, produces his best standup
           yet. Focusing on the tribulations of the Trump era and life after the
           loss of a loved one, Patton Oswald continues his journey to contribute
           joy to the world.
URL      : https://yts.am/movie/patton-oswalt-annihilation-2017
Torrent  : https://yts.am/torrent/download/EDE583C37097A78B22BCD01D932CBDB85031622A
Quality  : 1080p
###################################################################################
Title    : New York Doll
Year     : 2005
Rating   : 7.9
Summary  : A recovering alcoholic and recently converted Mormon, Arthur "Killer"
           Kane, of the rock band The New York Dolls, is given a chance at
           reuniting with his band after 30 years.
URL      : https://yts.am/movie/new-york-doll-2005
Torrent  : https://yts.am/torrent/download/F0A08A2A1E033940DE0F43AC3429792EEB6EA758
Quality  : 1080p
###################################################################################
Title    : Tom Segura: Disgraceful
Year     : 2018
Rating   : 7.5
Summary  : Comedian Tom Segura rants about funny things about pop culture and the
           way of living in 2018.
URL      : https://yts.am/movie/tom-segura-disgraceful-2018
Torrent  : https://yts.am/torrent/download/2E68311C00EF2728980760C458D600C2847C84E5
Quality  : 1080p
###################################################################################
Title    : The Secret Rules of Modern Living: Algorithms
Year     : 2015
Rating   : 7.5
Summary  : Professor Marcus du Sautoy demystifies the hidden world of algorithms
           and reveals where these 2,000-year-old problem solvers came from, how
           they work, and what they have achieved.
URL      : https://yts.am/movie/the-secret-rules-of-modern-living-algorithms-2015
Torrent  : https://yts.am/torrent/download/9AE81BE3E686B5004B2AAEF959467815F15E18D8
Quality  : 1080p
###################################################################################
Title    : Farewell Ferris Wheel
Year     : 2016
Rating   : 7.9
Summary  : Farewell, Ferris Wheel examines the link between America's carnival
           industry and a small Mexican town that legally provides one third of the
           carnival's labor. However, increased regulations are compromising this
           longstanding connection, putting both the industry and its workers in
           jeopardy.
URL      : https://yts.am/movie/farewell-ferris-wheel-2016
Torrent  : https://yts.am/torrent/download/EF4B897719A102928EF620772CAB2701FC46BF45
Quality  : 1080p
###################################################################################
Title    : Andre the Giant
Year     : 2018
Rating   : 8
Summary  : A look at the life and career of professional wrestler André Roussimoff,
           who gained notoriety in the 1980s as Andre the Giant.
URL      : https://yts.am/movie/andre-the-giant-2018
Torrent  : https://yts.am/torrent/download/0D20CD2D38021250B6C65A3C94532962960CFBBD
Quality  : 1080p
###################################################################################
Title    : Faces Places
Year     : 2017
Rating   : 8
Summary  : Agnes Varda, one of the leading lights of France's honored French New
           Wave cinema era, and professional photographer and muralist, J.R.,
           partake on a special art project. Together, they travel around France in
           a special box truck equipped as a portable photo booth and traveling
           printing facility as they take photographs of people around the country.
           With that inspiration, they also create special colossal mural pictures
           of individuals, communities and places they want to honor and celebrate.
           Along the way, the old cinematic veteran and the young artistic idealist
           enjoy an odd friendship as they chat and explore their views on the
           world as only they can.
URL      : https://yts.am/movie/faces-places-2017
Torrent  : https://yts.am/torrent/download/9A12ED3D763FF85E3A0FC4D7E6F1054126C24B7B
Quality  : 1080p
###################################################################################
Title    : Sons of Ben
Year     : 2016
Rating   : 7.7
Summary  : After many rumors of an MLS team arriving in Philadelphia never
           materializing, a small group of soccer fans took matters into their own
           hands and started a supporters group called the Sons of Ben to help
           bring a team to their hometown. They were a group without a team to root
           for and had a modest goal of reaching 100 members by the end of the
           year. Little did they know they would reach over 1,500 members in less
           time than that and start a movement that would not only change the
           soccer landscape in Philadelphia forever, but also help revive a
           community that had been struggling for decades.
URL      : https://yts.am/movie/sons-of-ben-2016
Torrent  : https://yts.am/torrent/download/21FF655570FF2769D303A722AC5D34FE69AEAB75
Quality  : 1080p
###################################################################################
Title    : The Redeemed and the Dominant: Fittest on Earth
Year     : 2018
Rating   : 7.1
Summary  : In 2017 the fittest athletes on Earth took on the unknown and unknowable
           during four of the most intense days of competition in CrossFit Games
           history. "The Redeemed and the Dominant: Fittest on Earth " captures all
           the drama as top athletes resembling chiseled Grecian gods descend on
           Madison, Wisconsin, to face a series of trials. Hercules faced 12; they
           take on 13. Emotions run high as a throng of Australian athletes rise to
           the top. By the end of the competition, some learn tough lessons-that
           all that glitters isn't gold, or even bronze-and some learn that they're
           even stronger than they realized. The best among them enter the pantheon
           of CrossFit giants and earn the right to call themselves the "Fittest on
           Earth."
URL      : https://yts.am/movie/the-redeemed-and-the-dominant-fittest-on-earth-2018
Torrent  : https://yts.am/torrent/download/B93B0BF64A8523860958C01AED7C73D301F039A2
Quality  : 1080p
###################################################################################
```

